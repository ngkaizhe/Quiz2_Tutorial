﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class AudioRecord : MonoBehaviour {

	public GameObject timeText;
	bool recording = false;
	AudioClip recordedClip;

	void Start() {
	}

	void Update() {
	}

	public void Record() {
		if (!recording) {
			recording = true;
			StartCoroutine(RecordCoroutine());
		}
	}

	IEnumerator RecordCoroutine() {
		AudioClip clip = Microphone.Start ("", false, 5, 44100);
		float time = 5.0f;
		WaitForSeconds wait = new WaitForSeconds (0.1f);
		while (time > 0.0f) {
			yield return wait;
			time -= 0.1f;
			timeText.GetComponent<Text>().text = time.ToString("F1");
		}
		timeText.GetComponent<Text> ().text = "0.0";
		Microphone.End ("");
		recording = false;
		recordedClip = clip;
	}

	public void Play() {
		AudioSource source = GetComponent<AudioSource> ();
		if (!source.isPlaying) {
			source.clip = recordedClip;
			source.Play();
		}
	}
}

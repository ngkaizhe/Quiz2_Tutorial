﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class AudioSourceController : MonoBehaviour {

	public GameObject nearAnchor;
	public GameObject farAnchor;
	public GameObject backAnchor;
	public AudioSource audioSource;
	public Text toggle2DText;
	public AudioClip clip;

	// Use this for initialization
	void Start () {
		MoveToNearAnchor ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void MoveToNearAnchor() {
		transform.position = nearAnchor.transform.position;
	}

	public void MoveToFarAnchor() {
		transform.position = farAnchor.transform.position;
	}

	public void Toggle2DSound() {
		if (audioSource.spatialBlend == 0.0f) {
			// currently 2D sound
			audioSource.spatialBlend = 1.0f;
			toggle2DText.text = "3D Sound";
		} else {
			// currently 3D sound
			audioSource.spatialBlend = 0.0f;
			toggle2DText.text = "2D Sound";
		}
	}

	public void SlowSound() {
		audioSource.pitch = 0.5f;
	}

	public void FastSound() {
		audioSource.pitch = 1.5f;
	}

	public void NormalSound() {
		audioSource.pitch = 1.0f;
	}

	public void DopplerEffect() {
		StartCoroutine (DopplerCoroutine ());
	}

	IEnumerator DopplerCoroutine() {
		WaitForEndOfFrame wait = new WaitForEndOfFrame ();
		Vector3 start = farAnchor.transform.position;
		Vector3 end = backAnchor.transform.position;
		float animTime = 1.0f;
		float time = 0.0f;
		float f = 0.0f;
		while(f <= 1.0f) {
			transform.position = start * (1.0f - f) + end * f;
			f = time / animTime;
			time += Time.deltaTime;
			yield return wait;
		}
		transform.position = end;
	}
}

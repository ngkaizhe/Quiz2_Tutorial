﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ReverbZoneController : MonoBehaviour {

	public GameObject startAnchor;
	public GameObject endAnchor;
	public GameObject character;

	// Use this for initialization
	void Start () {
		character.transform.position = startAnchor.transform.position;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void Walk() {
		StartCoroutine (WalkCoroutine ());
	}

	IEnumerator WalkCoroutine() {
		WaitForEndOfFrame wait = new WaitForEndOfFrame ();
		character.GetComponent<AudioSource> ().Play ();
		Vector3 start = startAnchor.transform.position;
		Vector3 end = endAnchor.transform.position;
		float animTime = 22.5f;
		float time = 0.0f;
		float f = 0.0f;
		while(f <= 1.0f) {
			character.transform.position = start * (1.0f - f) + end * f;
			f = time / animTime;
			time += Time.deltaTime;
			yield return wait;
		}
		character.transform.position = end;
		character.GetComponent<AudioSource> ().Stop ();
	}
}

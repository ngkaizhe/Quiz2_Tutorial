﻿using UnityEngine;
using System.Collections;

public class RealTimeMicrophone : MonoBehaviour {

	GameObject[] barCells;
	float[] samples;

	// Use this for initialization
	void Start () {
		InitializeBar ();
		StartCoroutine (RecordOneSec ());
	}

	void InitializeBar() {
		barCells = new GameObject[20];
		GameObject bar = transform.Find ("Bar").gameObject;
		GameObject cell = bar.transform.Find ("Cell").gameObject;
		barCells[0] = cell;
		for (int i = 1; i <= 19; i++) {
			GameObject copy = GameObject.Instantiate(cell);
			RectTransform copyRect = copy.GetComponent<RectTransform>();
			copyRect.SetParent(bar.transform);
			copyRect.anchoredPosition = new Vector2(15 + i * 25, 0);
			barCells[i] = copy;
		}
	}

	void SetBarValue(int val) {
		for (int i = 0; i < 20; i++) {
			barCells[i].SetActive(i <= val);
		}
	}

	IEnumerator RecordOneSec() {
		AudioClip clip = Microphone.Start ("", false, 1, 44100);
		yield return new WaitForSeconds (0.1f);
		Microphone.End ("");
		if (samples == null) {
			samples = new float[clip.samples * clip.channels];
		}
		clip.GetData (samples, 0);
		float max = 0.0f;
		for(int i = 0; i < clip.samples * clip.channels * 0.1f; i++) {
			max = Mathf.Max (max, samples[i]);
		}
		SetBarValue (Mathf.RoundToInt(max * 20));
		StartCoroutine (RecordOneSec ());
	}
}

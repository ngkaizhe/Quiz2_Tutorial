﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Skeleton_Controller : MonoBehaviour
{
    Animation m_animation;
    // Start is called before the first frame update
    void Start()
    {
        m_animation = GetComponent<Animation>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.A))
        {
            m_animation.Play("attack");
        }
    }
}

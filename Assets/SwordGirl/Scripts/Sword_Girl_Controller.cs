﻿using UnityEngine;
using System.Collections;

public class Sword_Girl_Controller : MonoBehaviour {
    Animator m_animator;

    public float m_speed = 0.0f;
    float max_speed = 1.0f;
    float min_speed = 0.0f;

    // Use this for initialization
    void Start () {
        m_animator = GetComponent<Animator>();
    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.A))
            m_animator.SetTrigger("attack");
        if (Input.GetKeyDown(KeyCode.S))
            m_animator.SetTrigger("jump");

        if(Input.GetKey(KeyCode.W))
        {
            m_speed = Mathf.Clamp(m_speed + Time.deltaTime, min_speed, max_speed);
            m_animator.SetFloat("speed", m_speed);
        }
        else
        {
            m_speed = Mathf.Clamp(m_speed - Time.deltaTime, min_speed, max_speed);
            m_animator.SetFloat("speed", m_speed);
        }
    }
}

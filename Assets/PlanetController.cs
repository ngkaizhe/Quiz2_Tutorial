﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanetController : MonoBehaviour
{
    public GameObject childPlanet;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void disappear()
    {
        childPlanet.SetActive(false);
    }

    public void show()
    {
        childPlanet.SetActive(true);
    }
}
